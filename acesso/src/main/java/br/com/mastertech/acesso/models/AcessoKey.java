package br.com.mastertech.acesso.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AcessoKey  implements Serializable {

    @Column
    private Integer porta_id;
    @Column
    private Long cliente_id;


    public void setPorta_id(Integer porta_id) {
        this.porta_id = porta_id;
    }
    public int getPorta_id() {
        return porta_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
