package br.com.mastertech.acesso.clients;

import br.com.mastertech.acesso.models.dto.ClienteDTO;

public class ClienteClientFallback implements  ClienteClient{
    @Override
    public ClienteDTO getById(Long id) {
        throw  new ClienteOffLineException();
    }
}
