package br.com.mastertech.acesso.controller;

import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.AcessoKey;
import br.com.mastertech.acesso.servico.AcessoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoServico acessoServico;

    @PostMapping()
    @ResponseStatus(code = HttpStatus.CREATED)
    public AcessoKey criarAcesso(@RequestBody AcessoKey acesso){
        return  acessoServico.salvaAcesso(acesso).getAcessoKey();
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Acesso deletarAcesso(@PathVariable long cliente_id , @PathVariable int porta_id) {
        return acessoServico.deletaAcesso(cliente_id, porta_id);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(code = HttpStatus.OK)
    public AcessoKey consultarAcesso(@PathVariable long cliente_id , @PathVariable Integer porta_id) {
        return acessoServico.getAcesso(cliente_id, porta_id).getAcessoKey();
    }

}
