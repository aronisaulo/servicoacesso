package br.com.mastertech.acesso.repositories;

import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.AcessoKey;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, AcessoKey> {

}
