package br.com.mastertech.acesso.servico;

import br.com.mastertech.acesso.clients.ClienteClient;
import br.com.mastertech.acesso.clients.PortaClient;
import br.com.mastertech.acesso.models.Acesso;
import br.com.mastertech.acesso.models.AcessoKey;
import br.com.mastertech.acesso.producer.AcessoKafkaDTO;
import br.com.mastertech.acesso.models.dto.ClienteDTO;
import br.com.mastertech.acesso.models.dto.PortaDTO;
import br.com.mastertech.acesso.producer.AcessoProducer;
import br.com.mastertech.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class AcessoServico {

    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    PortaClient portaClient;

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    AcessoProducer acessoProducer;

    public Acesso salvaAcesso(AcessoKey acesso){
        Acesso acessoV = verificarClient(acesso.getCliente_id(), acesso.getPorta_id());
        if (acessoV != null) {
      //      Acesso acessoGravar = new Acesso();
        //    acessoGravar.setAcessoKey(acesso);
            return acessoRepository.save(acessoV);
        }
        else
            throw new RuntimeException();
    }

    public Acesso deletaAcesso(long cliente_id, int porta_id) {
        Acesso acessoDeletar = this.getAcesso(cliente_id, porta_id);
        acessoRepository.delete(acessoDeletar);
        return acessoDeletar;
    }

    public Acesso getAcesso(long cliente_id, Integer porta_id){

        Acesso acesso = verificarClient(cliente_id, porta_id);
        Optional<Acesso> retorno = acessoRepository.findById(acesso.getAcessoKey());

        return retorno.get();
    }

    public Acesso verificarClient(long cliente_id, Integer porta_id){
        ClienteDTO c = clienteClient.getById(cliente_id);
        PortaDTO p =  portaClient.getById(porta_id);
        AcessoKey acesso = new AcessoKey();
        if ( p.getId() != 0 && c.getId() != 0 ) {
            acesso.setPorta_id(p.getId());
            acesso.setCliente_id((c.getId()));
        }

        Acesso acessoRetorno = new Acesso();
        acessoRetorno.setAcessoKey(acesso);

        // kafka
        AcessoKafkaDTO acessoKafkaDTO = new AcessoKafkaDTO();
        acessoKafkaDTO.setAutorizado(true);
        acessoKafkaDTO.setClienteId(acessoRetorno.getAcessoKey().getCliente_id());
        acessoKafkaDTO.setPorta_id(acessoRetorno.getAcessoKey().getPorta_id());
        acessoKafkaDTO.setDataVerificacao(new Date());

        acessoProducer.enviarAoKafka(acessoKafkaDTO);

        return acessoRetorno;
    }

}
