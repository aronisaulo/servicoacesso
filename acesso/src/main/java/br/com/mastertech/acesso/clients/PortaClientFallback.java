package br.com.mastertech.acesso.clients;

import br.com.mastertech.acesso.models.dto.PortaDTO;

public class PortaClientFallback implements PortaClient {
    @Override
    public PortaDTO getById(int id) {
        throw new PortaOffLineException();
    }
}
