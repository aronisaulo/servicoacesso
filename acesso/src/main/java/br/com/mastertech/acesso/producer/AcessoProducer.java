package br.com.mastertech.acesso.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {
    @Autowired
    private KafkaTemplate<String , AcessoKafkaDTO> producer;

    public void enviarAoKafka(AcessoKafkaDTO acesso){

        System.out.println("Kafka" + acesso.getDataVerificacao());

        producer.send("spec3-saulo-aroni-1", acesso);

    }
}
