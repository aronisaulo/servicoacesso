package br.com.mastertech.acesso.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY , reason = "Api de Porta indisponivel no momento")
public class PortaOffLineException extends RuntimeException {
}
