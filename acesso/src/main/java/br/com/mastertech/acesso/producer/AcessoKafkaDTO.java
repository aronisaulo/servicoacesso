package br.com.mastertech.acesso.producer;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class AcessoKafkaDTO {

    private Long clienteId;
    private int porta_id;
    private Boolean autorizado;
    @DateTimeFormat(pattern = "DD/mm/YYYY HH:nn:ss")
    private Date dataVerificacao;

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public int getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(int porta_id) {
        this.porta_id = porta_id;
    }

    public Boolean getAutorizado() {
        return autorizado;
    }

    public void setAutorizado(Boolean autorizado) {
        this.autorizado = autorizado;
    }

    public Date getDataVerificacao() {
        return dataVerificacao;
    }
    public void setDataVerificacao(Date dataVerificacao) {
        this.dataVerificacao = dataVerificacao;
    }


}
