package br.com.mastertech.acesso.models;

import javax.persistence.*;

@Entity
@Table
public class Acesso {

    @EmbeddedId
    private AcessoKey acessoKey;

    public AcessoKey getAcessoKey() {
        return acessoKey;
    }

    public void setAcessoKey(AcessoKey acessoKey) {
        this.acessoKey = acessoKey;
    }
}

