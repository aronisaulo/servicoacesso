package br.com.mastertech.cliente.controller;

import br.com.mastertech.cliente.models.dto.ClienteMapper;
import br.com.mastertech.cliente.models.dto.CreateClienteRequest;
import br.com.mastertech.cliente.service.ClienteService;
import br.com.mastertech.cliente.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService service;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente create(@RequestBody CreateClienteRequest createClienteRequest) {
        Cliente cliente = mapper.toCustomer(createClienteRequest);

        cliente = service.create(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Cliente getById(@PathVariable Long id) {
        return service.getById(id);
    }

}
