package br.com.mastertech.acesso.consumer;

import br.com.mastertech.acesso.producer.AcessoKafkaDTO;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-saulo-aroni-1", groupId = "Acesso-1")
    public void receber(@Payload AcessoKafkaDTO acesso) throws IOException {
        System.out.println("Recebi o log Cliente : " + acesso.getClienteId() + " Porta :" + acesso.getPorta_id() + " Data :" + acesso.getDataVerificacao() );
        AcessoLog acessoLog = new AcessoLog();
        acessoLog.Log(acesso);
    }

}
