package br.com.mastertech.acesso.consumer;

import br.com.mastertech.acesso.producer.AcessoKafkaDTO;


import java.io.*;

public class AcessoLog {

    public void Log(AcessoKafkaDTO acesso) throws IOException {
        Writer write = new FileWriter("acesso.csv",true);

        write.append("Data :" + acesso.getDataVerificacao().toString()+";");
        write.append("Cliente :" + acesso.getClienteId().toString()+";");
        write.append("Porta :" + String.valueOf(acesso.getPorta_id())+"\n\r");
        write.close();
    }
}
