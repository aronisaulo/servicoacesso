package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.models.Porta;
import br.com.mastertech.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    PortaService portaService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Porta criaPorta(@RequestBody Porta porta){
        return portaService.salvarPorta(porta);
    }

    @GetMapping("/{id}")
    public Porta getPorta(@PathVariable int id){
        return portaService.getPortaId(id);
    }
}
