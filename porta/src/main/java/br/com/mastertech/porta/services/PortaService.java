package br.com.mastertech.porta.services;

import br.com.mastertech.porta.models.Porta;
import br.com.mastertech.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    PortaRepository portaRepository;

    public Porta salvarPorta( Porta porta){
        return portaRepository.save(porta);
    }

    public Porta getPortaId(int id) {
        Optional<Porta> optionalPorta = portaRepository.findById(id);
        return optionalPorta.get();
    }
}
